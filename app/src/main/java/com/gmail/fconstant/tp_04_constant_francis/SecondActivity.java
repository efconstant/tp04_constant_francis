package com.gmail.fconstant.tp_04_constant_francis;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class SecondActivity extends AppCompatActivity {

    private static final String TAG = "DEBUG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        logCycleVie("onCreate");

        Button buttonFermer;
        buttonFermer = findViewById(R.id.buttonFermer);

        // Intercept click on the compute button
        buttonFermer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    /**
     onStart
     */
    @Override
    public void onStart() {
        super.onStart();
        logCycleVie("onStart");
    }

    @Override
    public void onResume(){
        super.onResume();
        logCycleVie("onResume");

    }

    @Override
    protected void onPause(){
        super.onPause();
        logCycleVie("onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        logCycleVie("onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        logCycleVie("onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        logCycleVie("onDestroy");
    }

    //methode pour enregistrer dans le log
    private void logCycleVie(String methode){
        Log.d(TAG, "TP04-SecondActivity- La méthode "+methode+" est appelée");
    }
}